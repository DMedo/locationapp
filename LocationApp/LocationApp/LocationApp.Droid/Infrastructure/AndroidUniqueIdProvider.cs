using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using LocationApp.Droid.Infrastructure;
using LocationApp.Infrastructure;
using Xamarin.Forms;

[assembly: Dependency(typeof(AndroidUniqueIdProvider))]
namespace LocationApp.Droid.Infrastructure
{
    
    public class AndroidUniqueIdProvider : IUniqueIdProvider
    {
        public string GetUniqueId()
        {
            var telephonyManager = Forms.Context.GetSystemService(Context.TelephonyService) as Android.Telephony.TelephonyManager;

            return telephonyManager.DeviceId;
        }
    }
}