using LocationApp.Infrastructure;
using LocationApp.iOS.Infrastructure;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(iOSUniqueIdProvider))]
namespace LocationApp.iOS.Infrastructure
{
    
    public class iOSUniqueIdProvider : IUniqueIdProvider
    {
        public string GetUniqueId()
        {
            return UIDevice.CurrentDevice.IdentifierForVendor.ToString();
        }
    }
}