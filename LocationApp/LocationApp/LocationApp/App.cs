﻿using LocationApp.Infrastructure;
using LocationApp.Pages;
using Xamarin.Forms;

namespace LocationApp
{
    public class App : Application
    {
        public IApiClient ApiClient { get; }
        public DbClient DbClient { get; }
        public bool IsAppInBackground => _isAppInBackground;

        private bool _isAppInBackground;

        public App()
        {
#if DEBUG
            ApiClient = new MockApiClient();
#else
            ApiClient = new ApiClient();
#endif
            DbClient = new DbClient();

            var uniqueId = DbClient.GetUniqueId();

            if (!string.IsNullOrWhiteSpace(uniqueId))
            {
                ApiClient.SetAuthHeader(uniqueId);
                MainPage = new Map();

                return;
            }

            MainPage = new Login();
        }

        protected override void OnStart()
        {
            _isAppInBackground = false;
        }

        protected override void OnSleep()
        {
            _isAppInBackground = true;
        }

        protected override void OnResume()
        {
            _isAppInBackground = false;
        }
    }
}
