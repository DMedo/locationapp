﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocationApp.Models.Contracts
{
    public class LocationModel
    {
        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public bool IsAppInBackground { get; set; }

        public DateTime Time { get; set; }
    }
}
