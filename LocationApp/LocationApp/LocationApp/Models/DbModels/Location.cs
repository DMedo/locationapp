﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Realms;

namespace LocationApp.Models
{
    public class Location : RealmObject
    {
        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public bool IsAppInBackground { get; set; }

        public DateTimeOffset Time { get; set; }
    }
}
