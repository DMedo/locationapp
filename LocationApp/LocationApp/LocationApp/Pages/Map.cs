﻿using System;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace LocationApp.Pages
{
    public class Map : Base
    {

        public Map() : base()
        {
            var map = new Xamarin.Forms.Maps.Map(
                MapSpan.FromCenterAndRadius(new Position(37, -122), Distance.FromMiles(0.3)))
                {
                    IsShowingUser = true,
                    HeightRequest = 100,
                    WidthRequest = 960,
                    VerticalOptions = LayoutOptions.FillAndExpand
                };

            var stack = new StackLayout { Spacing = 0 };
            stack.Children.Add(map);
            Content = stack;

            SetCurrentLocation(map);

            Device.StartTimer(TimeSpan.FromSeconds(60), () =>
            {
                Task.Run(async () =>
                {
                    var locations = this.DbClient.GetLocations();

                    var success = await this.ApiClient.PostLocations(locations);

                    if (success)
                    {
                        this.DbClient.DeleteLocations(locations);
                    }
                });

                return true;
            });
        }

        public async Task SetCurrentLocation(Xamarin.Forms.Maps.Map map)
        {
            var locator = CrossGeolocator.Current;
            var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);

            var pinPosition = new Position(position.Latitude, position.Longitude);
            var pin = new Pin
            {
                Type = PinType.Generic,
                Position = pinPosition,
                Label = "Current position"
            };

            map.Pins.Add(pin);
            try
            {
                map.MoveToRegion(new MapSpan(map.VisibleRegion.Center, position.Latitude, position.Longitude));
            }
            catch (System.Exception e)
            {
            }            

            locator.PositionChanged += Locator_PositionChanged;
            await locator.StartListeningAsync(0, 1);
        }

        private void Locator_PositionChanged(object sender, Plugin.Geolocator.Abstractions.PositionEventArgs e)
        {
            this.DbClient.InsertLocation(e.Position.Longitude, e.Position.Latitude, IsAppInBackground);
        }
    }
}
