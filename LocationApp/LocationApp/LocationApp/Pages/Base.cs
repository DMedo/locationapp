﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using LocationApp.Infrastructure;
using Xamarin.Forms;

namespace LocationApp.Pages
{
    public class Base : ContentPage
    {
        protected IApiClient ApiClient;
        protected DbClient DbClient;
        protected bool IsAppInBackground => _app.IsAppInBackground;

        private App _app;

        public Base()
        {
            _app = Application.Current as App;
            ApiClient = _app.ApiClient;
            DbClient = _app.DbClient;
        }
    }
}
