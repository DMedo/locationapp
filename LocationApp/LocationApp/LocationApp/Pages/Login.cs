﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using LocationApp.Infrastructure;
using Realms;
using Xamarin.Forms;

namespace LocationApp.Pages
{
    public class Login : Base
    {
        private Entry _username;
        private Picker _sex;
        private string _appUniqueCode;

        public Login() : base()
        {
            var layout = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.Center                
            };

            layout.Children.Add(new Label { Text = "Name:" });

            _username = new Entry();
            layout.Children.Add(_username);
            layout.Children.Add(new Label { Text = "Sex:" });

            _sex = new Picker();
            _sex.Items.Add("Male");
            _sex.Items.Add("Female");

            layout.Children.Add(_sex);

            var button = new Button { Text = "Login" };
            button.Clicked += Button_Clicked;

            layout.Children.Add(button);

            Content = layout;
            Padding = new Thickness(20);
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            _appUniqueCode = DependencyService.Get<IUniqueIdProvider>().GetUniqueId();

            var uniqueId = await this.ApiClient.Login(_username.Text, _sex.Items[_sex.SelectedIndex], _appUniqueCode);

            if (!string.IsNullOrWhiteSpace(uniqueId))
            {
                this.DbClient.InsertUniqueId(uniqueId);
                this.ApiClient.SetAuthHeader(uniqueId);

                await this.Navigation.PushModalAsync(new Map());
            }
        }
    }
}
