﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LocationApp.Models;
using Realms;
using Xamarin.Forms;

namespace LocationApp.Infrastructure
{
    public class DbClient
    {
        private Realm _realm;
        private RealmConfiguration _realmConfig;

        public DbClient()
        {
            _realmConfig = new RealmConfiguration(shouldDeleteIfMigrationNeeded: true);
        }

        public void InsertUniqueId(string uniqueId)
        {
            CreateNewRealm();
            _realm.Write(() =>
            {
                var token = _realm.CreateObject<Token>();
                token.UniqueId = uniqueId;
            });
        }

        private void CreateNewRealm()
        {
            _realm = Realm.GetInstance(_realmConfig);
        }

        public string GetUniqueId()
        {
            CreateNewRealm();

            return _realm.All<Token>()
                .ToList()
                .FirstOrDefault()
                ?.UniqueId;
        }

        public void InsertLocation(double longitude, double latitude, bool isAppInBackground)
        {
            CreateNewRealm();

            _realm.Write(() =>
            {
                var location = _realm.CreateObject<Location>();
                location.Longitude = longitude;
                location.Latitude = latitude;
                location.IsAppInBackground = isAppInBackground;
                location.Time = DateTime.Now;
            });
        }

        public List<Location> GetLocations()
        {
            CreateNewRealm();

            return _realm.All<Location>().ToList();            
        }

        public void DeleteLocations(List<Location> locations)
        {
            CreateNewRealm();

            try
            {
                using (var transaction = _realm.BeginWrite())
                {
                    foreach (var location in locations)
                    {
                        _realm.Remove(location);
                    }

                    transaction.Commit();
                }
            }
            catch (Exception e)
            {
            }
           
        }
    }
}
