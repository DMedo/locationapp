﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LocationApp.Models;

namespace LocationApp.Infrastructure
{
    public interface IApiClient
    {
        void SetAuthHeader(string authHeader);

        Task<string> Login(string name, string sex, string uniqueId);

        Task<bool> PostLocations(List<Location> locations);
    }
}
