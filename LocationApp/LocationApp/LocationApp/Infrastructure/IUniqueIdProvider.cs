﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocationApp.Infrastructure
{
    public interface IUniqueIdProvider
    {
        string GetUniqueId();
    }
}
