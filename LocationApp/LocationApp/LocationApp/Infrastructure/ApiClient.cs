﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using LocationApp.Models;
using LocationApp.Models.Contracts;
using Newtonsoft.Json;

namespace LocationApp.Infrastructure
{
    public class ApiClient : IApiClient
    {
        private HttpClient _client;
        private string _baseUri = "http://10.0.2.2:61939/api/location";
        private string _authHeader;

        public ApiClient()
        {
            _client = new HttpClient();
        }

        public void SetAuthHeader(string authHeader)
        {
            _authHeader = authHeader;
        }

        public async Task<string> Login(string name, string sex, string uniqueId)
        {
            var loginModel = new LoginModel
            {
                Name = name,
                Sex = sex,
                UniqueId = uniqueId
            };

            var response = await this.PostAsync("login", loginModel);

            if (response.IsSuccessStatusCode)
            {
                var responseString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<string>(responseString);
            }

            return null;
        }

        public async Task<bool> PostLocations(List<Location> locations)
        {
            var locationModels = locations.Select(x => new LocationModel
            {
                Latitude = x.Latitude,
                Longitude = x.Longitude,
                IsAppInBackground = x.IsAppInBackground
            });

            var response = await this.PostAsync("update", locationModels);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        private async Task<HttpResponseMessage> PostAsync(string path, object model)
        {
            var uri = this.GetUri(path);
            var json = JsonConvert.SerializeObject(model);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            if (!string.IsNullOrWhiteSpace(_authHeader))
            {
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", _authHeader);
            }

            var response = await _client.PostAsync(uri, content);

            return response;
        }

        private Uri GetUri(string path)
        {
            return new Uri($"{_baseUri}/{path}");
        }
    }
}
