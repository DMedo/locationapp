﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using LocationApp.Models;
using LocationApp.Models.Contracts;
using Newtonsoft.Json;

namespace LocationApp.Infrastructure
{
    public class MockApiClient : IApiClient
    {
        private HttpClient _client;
        private string _baseUri = "http://10.0.2.2:61939/api/location";
        private string _authHeader;

        public MockApiClient()
        {
        }

        public void SetAuthHeader(string authHeader)
        {
        }

        public async Task<string> Login(string name, string sex, string uniqueId)
        {
            return await Task.FromResult("uniqueIdCodeMock");
        }

        public async Task<bool> PostLocations(List<Location> locations)
        {
            return await Task.FromResult(true);
        }
    }
}
