﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocationApi.Models.Contracts
{
    public class LocationModel
    {
        public float Latitude { get; set; }

        public float Longitude { get; set; }

        public bool IsAppInBackground { get; set; }
    }
}
