﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LocationApi.Models.Db
{
    public class Location : EntityBase
    {
        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public bool IsAppInBackground { get; set; }

        public int AuthTokenId { get; set; }
    }
}