﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LocationApi.Models.Db
{
    public class AuthToken : EntityBase
    {
        public string AuthenticationToken { get; set; }

        public string UniqueId { get; set; }

        public string Name { get; set; }

        public string Sex { get; set; }

        public ICollection<Location> Locations { get; set; }
    }
}