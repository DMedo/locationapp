﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LocationApi.Models.Db
{
    public class EntityBase
    {
        [Key]
        public int Id { get; set; }
    }
}