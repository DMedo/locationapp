﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using LocationApi.Models.Db;

namespace LocationApi.Infrastructure
{
    public class AppDbContext : DbContext
    {
        public DbSet<AuthToken> AuthTokens { get; set; }
        public DbSet<Location> Locations { get; set; }
    }
}