﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace LocationApi.Infrastructure
{
    public class ApiAuthorize : AuthorizeAttribute
    {
        protected override bool IsAuthorized(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var request = actionContext.Request;

            var authHeader = actionContext.Request.Headers.GetValues("Authorization").FirstOrDefault();
            if (authHeader != null)
            {
                var authHeaderValue = AuthenticationHeaderValue.Parse(authHeader);
                
                if (authHeaderValue.Scheme.Equals("basic", StringComparison.OrdinalIgnoreCase) 
                    && authHeaderValue.Parameter != null)
                {
                    using (var db = new AppDbContext())
                    {
                        var authToken = db.AuthTokens
                            .Where(x => x.AuthenticationToken == authHeaderValue.Parameter)
                            .FirstOrDefault();

                        if (authToken != null)
                        {
                            var principal = new UserPrincipal(new GenericIdentity(authToken.AuthenticationToken), new string[] { string.Empty });
                            principal.AuthTokenId = authToken.Id;
                            Thread.CurrentPrincipal = principal;
                            HttpContext.Current.User = principal;

                            return true;
                        }
                    }
                }
            }

            return false;
        }
    }
}