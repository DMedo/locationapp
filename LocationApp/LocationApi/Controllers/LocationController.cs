﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using LocationApi.Infrastructure;
using LocationApi.Models.Contracts;
using LocationApi.Models.Db;

namespace LocationApi.Controllers
{
    public class LocationController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Test()
        {
            return Ok("bok");
        }

        [HttpPost]
        public IHttpActionResult Login([FromBody]LoginModel model)
        {
            using (var db = new AppDbContext())
            {
                var authToken = new AuthToken
                {
                    AuthenticationToken = Guid.NewGuid().ToString(),
                    Name = model.Name,
                    Sex = model.Sex,
                    UniqueId = model.UniqueId
                };                
                
                db.AuthTokens.Add(authToken);
                db.SaveChanges();

                return this.Ok(authToken.AuthenticationToken);
            }
        }

        [ApiAuthorize]
        [HttpPost]
        public IHttpActionResult Login([FromBody]List<LocationModel> model)
        {
            using (var db = new AppDbContext())
            {                
                foreach (var location in model)
                {
                    var dbLocation = new Location
                    {
                        IsAppInBackground = location.IsAppInBackground,
                        Latitude = location.Latitude,
                        Longitude = location.Longitude,
                        AuthTokenId = (Thread.CurrentPrincipal as UserPrincipal).AuthTokenId
                    };

                    db.Locations.Add(dbLocation);
                }

                db.SaveChanges();
            }

            return this.Ok();
        }
    }
}
